<?php
/**
 * Implements hook_menu().
 */
function content_newsletter_menu() {
  $items['admin/config/system/content_newsletter/settings'] = array(
    'title' => 'Content Newsletter settings',
    'description' => 'Configure relevance settings for Content Newsletter.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('content_newsletter_admin_settings'),
    'access arguments' => array('administer content types'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => -10,
  );

  $items['newsletter/html/%'] = array(
      'type' => MENU_NORMAL_ITEM,
      'page callback' => '_newsletter_html',
      'page arguments' => array(2,3),
      'access arguments' => array('access content'),
  );

  return $items;
}

/**
 * Implements hook_custom_theme.
 */
function content_newsletter_custom_theme() {
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));
    if ($node->type == variable_get("type_content_newsletter")) {
      return variable_get('admin_theme');
    }
  }
}

function _newsletter_html($nid,$mode) {
  if ($mode == 'plain') {
    $newsletter = file_get_contents(url('newsletter/html/'.$nid.'/code', array('absolute' => true)));
    header('Content-disposition: attachment; filename=Newsletter.txt');
    header('Content-type: text/plain');

    print $newsletter;
    drupal_exit();
  }
  return $nid;
}

function content_newsletter_admin_settings($form) {

  $type = variable_get("type_content_newsletter");
  $field = variable_get("field_ref_content_newsletter");
  $settings = variable_get('content_newsletter_settings');

  $content_types = node_type_get_types();
  $field_ref = field_info_field($field);


  foreach ($content_types as $key => $value) {
    $arr_types[$key] = $value->name;

    $fie = field_info_instances('node', $key);
    foreach($fie as $k => $f) {
      if ($f['widget']['module'] == 'image') {
        $fields[$key][$k] = $f['label'];
      }
    }
  }

  unset($arr_types[$type]);

  $readonly = array();
  $aws_module = '';
  if (!module_exists('awssdk2')) {
    $readonly = array('readonly' => 'readonly');
    $aws_module = ' (Awssdk2 module is disabled)';
  }

  $form['content_types'] = array(
    '#type' => 'checkboxes',
    '#options' => $arr_types,
    '#default_value' => (!empty(array_keys($field_ref['settings']['handler_settings']['target_bundles']))) ? array_keys($field_ref['settings']['handler_settings']['target_bundles']) : '',
    '#title' => t('Select content types that filter on entity reference of Content Newsletter'),
  );

  foreach ($arr_types as $key => $value) {
    if (!empty($fields[$key])) {
      $form['field_image_'.$key] = array(
        '#type' => 'radios',
        '#options' => $fields[$key],
        '#title' => t('Select image field of @content that show on newsletter', array('@content' => $value)),
        '#states' => array(
          'visible' => array(
            ':input[name="content_types['.$key.']"]' => array('checked' => TRUE),
          ),
        ),
        '#default_value' => (!empty($settings['images'][$key])) ? $settings['images'][$key] : '',
      );
    }
  }

  $form['awsdk2'] = array(
    '#type' => 'checkbox',
    '#title' => t('Upload images on Amazon AWS.') . $aws_module,
    '#attributes' => $readonly,
    '#description' => t('Check this field if use Amazon AWS to upload images of Newsletter'),
    '#default_value' => (!empty($settings['awsdk2'])) ? $settings['awsdk2'] : '',
  );

  $form['awsdk2_bucket'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of bucket.'),
    '#attributes' => $readonly,
    '#description' => t('Check this field if use Amazon AWS to upload images of Newsletter'),
    '#default_value' => (!empty($settings['awsdk2_bucket'])) ? $settings['awsdk2_bucket'] : '',
    '#states' => array(
      'visible' => array(
        ':input[name="awsdk2"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['#submit'][] = 'content_newsletter_admin_settings_submit';

  return system_settings_form($form);
}

function content_newsletter_admin_settings_submit($form, &$form_state) {
  $field_ref = field_info_field(variable_get("field_ref_content_newsletter"));

  $arr_val = array_values($form_state['values']['content_types']);
  $arr_val = array_combine($arr_val,$arr_val);
  if (array_key_exists(0, $arr_val)) {
    unset($arr_val[0]);
  }

  $settings = array();

  $settings['awsdk2'] = $form_state['input']['awsdk2'];
  $settings['awsdk2_bucket'] = $form_state['input']['awsdk2_bucket'];

  foreach ($arr_val as $k => $v) {
    if (!empty($form_state['input']['field_image_'.$k])) {
      $settings['images'][$k] = $form_state['input']['field_image_'.$k];
    }
  }

  // Save values of form
  variable_set('content_newsletter_settings',$settings);

  // Syncronize with entity reference field
  $field_ref['settings']['handler_settings']['target_bundles'] = $arr_val;

  field_update_field($field_ref);
}

/**
 * Tell Drupal to also look for template files in the modules folder
 *
 * implements hook_theme_registry_alter
 *
 * @param $theme_registry
 */
function content_newsletter_theme_registry_alter(&$theme_registry) {
  $path  = drupal_get_path('module', 'content_newsletter'). '/templates';
  $templates = drupal_find_theme_templates($theme_registry, '.tpl.php', $path);

  foreach ($templates as $key => $value) {
    $templates[$key]['type'] = 'module';
  }
  $theme_registry += $templates;
}

function content_newsletter_preprocess_html(&$variables) {
  if (arg(0) == 'newsletter' && arg(1) == 'html') {

    $variables['node'] = node_load(arg(2));

    $message_url  = url('newsletter/html/'.arg(2), array('absolute' => true));
    $variables['message_click'] = t(
      'Email not displaying correctly? <a href="@message-url" target="_blank">View it in your browser</a>.',
      array(
        '@message-url' => $message_url,
      )
    );

    // Content referenced variables
    $settings = variable_get('content_newsletter_settings');
    if (!empty($variables['node']->field_content_newsletter_ref[LANGUAGE_NONE])) {
      foreach ($variables['node']->field_content_newsletter_ref[LANGUAGE_NONE] as $value) {
        $node_ref = node_load($value['target_id']);

        $variables['content_referenced'][$node_ref->nid]['title'] = $node_ref->title;
        $variables['content_referenced'][$node_ref->nid]['body'] = $node_ref->body;
        $options = array('absolute' => TRUE);
        $variables['content_referenced'][$node_ref->nid]['link'] = url('node/' . $node_ref->nid, $options);

        if (in_array($node_ref->type, array_keys($settings['images']))) {
          if(arg(3) == 'code' && !empty($settings['awsdk2'])) {
            $client = awssdk_get_client('s3');
            $type = variable_get("type_content_newsletter");
            $result = $client->putObject(array(
                'Bucket' => $settings['awsdk2_bucket'],
                'ContentType' => $node_ref->{$settings['images'][$node_ref->type]}['und'][0]['filemime'],
                'ACL'    => 'public-read',
                'StorageClass' => 'REDUCED_REDUNDANCY',
                'Key'    => $type . '/' . $variables['node']->nid . '/' . $node_ref->{$settings['images'][$node_ref->type]}['und'][0]['filename'],
                'SourceFile' => drupal_realpath($node_ref->{$settings['images'][$node_ref->type]}['und'][0]['uri']),
            ));

            $variables['content_referenced'][$node_ref->nid]['image'] = $result['ObjectURL'];
          }
          else {
            $variables['content_referenced'][$node_ref->nid]['image'] = file_create_url($node_ref->{$settings['images'][$node_ref->type]}['und'][0]['uri']);
          }
        }
      }
    }
  }
}

function content_newsletter_preprocess_node(&$variables) {
  $type = variable_get("type_content_newsletter");
  if ($variables['node']->type == $type) {

    drupal_add_css(drupal_get_path('module', 'content_newsletter') . '/css/content_newsletter.css', array('group' => CSS_DEFAULT, 'every_page' => FALSE));
    $newsletter_url  = url('newsletter/html/'.arg(1), array('absolute' => true));
    $variables['newsletter_url'] = $newsletter_url;
    $variables['link_html_optain'] = l(t('Obtain HTML code'),$newsletter_url.'/plain', array('attributes' => array('target' => '_blank')));
  }
}
